function Validation() {
  this.kiemTraRong = function (value, selectorError, name) {
    if (value.trim() === "") {
      document.querySelector(
        selectorError
      ).innerHTML = `${name} không thể để trống !`;
      return false;
    } else {
      document.querySelector(selectorError).innerHTML = "";
      return true;
    }
  };

  this.kiemTraDoDai = function (value, selectorError, max, min, name) {
    if (value.length < min || value.length > max) {
      document.querySelector(
        selectorError
      ).innerHTML = `${name} phải từ ${min} đến ${max} ký số !`;
      return false;
    } else {
      document.querySelector(selectorError).innerHTML = "";
      return true;
    }
  };

  this.kiemTraKySo = function (value, selectorError, name) {
    var regexAllNumber = /^[0-9]+$/;
    if (regexAllNumber.test(value)) {
      document.querySelector(selectorError).innerHTML = "";
      return true;
    }

    document.querySelector(
      selectorError
    ).innerHTML = `${name} bắt buộc phải là ký số !`;
    return false;
  };

  this.kiemTraKiTu = function (value, selectorError, name) {
    var regexAllCharacter = /^[A-Z a-z]+$/;
    if (regexAllCharacter.test(value)) {
      document.querySelector(selectorError).innerHTML = "";
      return true;
    }

    document.querySelector(
      selectorError
    ).innerHTML = `${name} bắt buộc phải là ký tự !`;
    return false;
  };

  this.kiemTraMinMaxGiaTri = function (
    value,
    selectorError,
    maxGiaTri,
    minGiaTri,
    name
  ) {
    if (Number(value) < minGiaTri || Number(value) > maxGiaTri) {
      document.querySelector(
        selectorError
      ).innerHTML = `${name} phải từ ${minGiaTri} đến ${maxGiaTri} !`;
      return false;
    } else {
      document.querySelector(selectorError).innerHTML = "";
      return true;
    }
  };
}

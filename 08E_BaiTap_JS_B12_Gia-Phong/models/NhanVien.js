function NhanVien() {
  this.maNhanVien = "";
  this.tenNhanVien = "";
  this.chucVu = "";
  this.heSoChucVu = "";
  this.luongCoBan = "";
  this.soGioLamTrongThang = "";
  this.tinhTongLuong = function () {
    var tongLuong =
      Number(this.luongCoBan) *
      Number(this.soGioLamTrongThang) *
      Number(this.heSoChucVu);
    return tongLuong;
  };
  this.xepLoaiTrongThang = function () {
    var xepLoai = "";
    if (Number(this.soGioLamTrongThang) > 120) {
      xepLoai = "Nhân viên xuất sắc";
    } else if (Number(this.soGioLamTrongThang) > 100) {
      xepLoai = "Nhân viên giỏi";
    } else if (Number(this.soGioLamTrongThang) > 80) {
      xepLoai = "Nhân viên khá";
    } else if (Number(this.soGioLamTrongThang) > 50) {
      xepLoai = "Nhân viên trung bình";
    } else {
      xepLoai = "Không thể xếp loại";
    }
    return xepLoai;
  };
}

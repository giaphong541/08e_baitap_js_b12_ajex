var kiemTra = new Validation();
// var arrNhanVien = [];

document.querySelector("#btnThemNhanVien").onclick = function () {
  var nhanVien = new NhanVien();
  nhanVien.maNhanVien = document.querySelector("#maNhanVien").value;
  nhanVien.tenNhanVien = document.querySelector("#tenNhanVien").value;
  nhanVien.heSoChucVu = document.querySelector("#chucVu").value;
  nhanVien.luongCoBan = document.querySelector("#luongCoBan").value;
  nhanVien.soGioLamTrongThang = document.querySelector(
    "#soGioLamTrongThang"
  ).value;

  var arrChucVu = document.querySelector("#chucVu");
  var selectedChucVu = arrChucVu.selectedIndex;
  nhanVien.chucVu = arrChucVu[selectedChucVu].innerHTML;

  var valid = true;

  // Kiểm tra rỗng
  valid &=
    kiemTra.kiemTraRong(
      nhanVien.maNhanVien,
      "#error_required_maNhanVien",
      "Mã nhân viên"
    ) &
    kiemTra.kiemTraRong(
      nhanVien.tenNhanVien,
      "#error_required_tenNhanVien",
      "Tên nhân viên"
    ) &
    kiemTra.kiemTraRong(
      nhanVien.luongCoBan,
      "#error_required_luongCoBan",
      "Lương cơ bản"
    ) &
    kiemTra.kiemTraRong(
      nhanVien.soGioLamTrongThang,
      "#error_required_soGioLamTrongThang",
      "Số giờ làm trong tháng"
    );

  // Kiểm tra loại dữ liệu
  valid &=
    kiemTra.kiemTraKySo(
      nhanVien.maNhanVien,
      "#error_allNumber_maNhanVien",
      "Mã nhân viên"
    ) &
    kiemTra.kiemTraKiTu(
      nhanVien.tenNhanVien,
      "#error_allLetters_tenNhanVien",
      "Tên nhân viên"
    ) &
    kiemTra.kiemTraKySo(
      nhanVien.luongCoBan,
      "#error_allNumber_luongCoBan",
      "Lương cơ bản"
    ) &
    kiemTra.kiemTraKySo(
      nhanVien.soGioLamTrongThang,
      "#error_allNumber_soGioLamTrongThang",
      "Số giờ làm trong tháng"
    );

  // Kiểm tra min max
  valid &=
    kiemTra.kiemTraDoDai(
      nhanVien.maNhanVien,
      "#error_Min_Max_maNhanVien",
      6,
      4,
      "Mã nhân viên"
    ) &
    kiemTra.kiemTraMinMaxGiaTri(
      nhanVien.luongCoBan,
      "#error_Min_Max_luongCoBan",
      20000000,
      1000000,
      "Lương cơ bản"
    ) &
    kiemTra.kiemTraMinMaxGiaTri(
      nhanVien.soGioLamTrongThang,
      "#error_Min_Max_soGioLamTrongThang",
      150,
      50,
      "Số giờ làm trong tháng"
    );

  if (!valid) {
    return;
  }

  console.log("nhanVien", nhanVien);
  var promise = axios({
    url: "http://svcy.myclass.vn/api/QuanLyNhanVienApi/ThemNhanVien",
    method: "post",
    data: nhanVien,
  });

  promise.then(function (result) {
    console.log("result", result.data);
    layDanhSachNhanVienAPI();
  });

  promise.catch(function (error) {
    console.log("error", error.response.data);
  });

  // arrNhanVien.push(nhanVien);
  // console.log("arrNhanVien", arrNhanVien);
  // renderTableNhanVien(arrNhanVien);
};

function layDanhSachNhanVienAPI() {
  var promise = axios({
    url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/LayDanhSachNhanVien`,
    method: "get",
    responseType: "json",
  });

  promise.then(function (result) {
    renderTableNhanVien(result.data);
  });

  promise.catch(function (error) {
    console.log(error.response.data);
  });
}

layDanhSachNhanVienAPI();

function renderTableNhanVien(arrNV) {
  var nVien = new NhanVien();
  var content = "";

  for (var index = 0; index < arrNV.length; index++) {
    var nhanVien = arrNV[index];
    nVien.maNhanVien = nhanVien.maNhanVien;
    nVien.tenNhanVien = nhanVien.tenNhanVien;
    nVien.heSoChucVu = nhanVien.heSoChucVu;
    nVien.chucVu = nhanVien.chucVu;
    nVien.luongCoBan = nhanVien.luongCoBan;
    nVien.soGioLamTrongThang = nhanVien.soGioLamTrongThang;


    // Tránh tình trạng giá trị luongCoBan không có
    if (nVien.luongCoBan !== null) {
      var stringLuongCoBan = nVien.luongCoBan
        .toString()
        .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }

    var stringTongLuong = nVien
      .tinhTongLuong()
      .toString()
      .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

    content += `
    <tr class="my-2">
    <td>${nVien.maNhanVien}</td>
    <td>${nVien.tenNhanVien}</td>
    <td>${nVien.chucVu}</td>
    <td>${stringLuongCoBan}</td>
    <td>${stringTongLuong}</td>
    <td>${nVien.soGioLamTrongThang}</td>
    <td>${nVien.xepLoaiTrongThang()}</td>
    <td>
    <button class="btn btn-danger" onclick="xoaNhanVien('${
      nVien.maNhanVien
    }')">Xóa</button>
    <button class="btn btn-success ml-2" onclick="chinhSua('${
      nVien.maNhanVien
    }')">Chỉnh sửa</button>
    </td>
    </tr>`;
  }

  document.querySelector("#tblNhanVien").innerHTML = content;
}

function xoaNhanVien(maNVClick) {
  var promise = axios({
    url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/XoaNhanVien?maSinhVien=${maNVClick}`,
    method: "delete",
    responseType: "json",
  });

  promise.then(function (result) {
    console.log(result.data);
    layDanhSachNhanVienAPI();
  });

  promise.catch(function (error) {
    console.log(error.response.data);
  });
}

function chinhSua(maNVClick) {
  var promise = axios({
    url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/LayThongTinNhanVien?maNhanVien=${maNVClick}`,
    method: "get",
    responseType: "json",
  });

  promise.then(function (result) {
    var nhanVien = result.data;
    console.log(nhanVien);
    document.querySelector("#maNhanVien").value = nhanVien.maNhanVien;
    document.querySelector("#tenNhanVien").value = nhanVien.tenNhanVien;
    document.querySelector("#chucVu").value = nhanVien.heSoChucVu;
    document.querySelector("#luongCoBan").value = nhanVien.luongCoBan;
    document.querySelector("#soGioLamTrongThang").value =
      nhanVien.soGioLamTrongThang;
  });

  promise.catch(function (error) {
    console.log(error.response.data);
  });

  document.querySelector("#btnCapNhatNhanVien").style.display = "inline-block";
  document.querySelector("#btnThemNhanVien").style.display = 'none';
  document.querySelector("#maNhanVien").disabled = true;
}

document.querySelector("#btnCapNhatNhanVien").onclick = function () {
  var nhanVienCapNhat = new NhanVien();
  nhanVienCapNhat.maNhanVien = document.querySelector("#maNhanVien").value;
  nhanVienCapNhat.tenNhanVien = document.querySelector("#tenNhanVien").value;
  nhanVienCapNhat.heSoChucVu = document.querySelector("#chucVu").value;
  nhanVienCapNhat.luongCoBan = document.querySelector("#luongCoBan").value;
  nhanVienCapNhat.soGioLamTrongThang = document.querySelector(
    "#soGioLamTrongThang"
  ).value;

  var arrChucVu = document.querySelector("#chucVu");
  var selectedChucVu = arrChucVu.selectedIndex;
  nhanVienCapNhat.chucVu = arrChucVu[selectedChucVu].innerHTML;
  
  var valid = true;

  // Kiểm tra rỗng
  valid &=
    kiemTra.kiemTraRong(
      nhanVienCapNhat.tenNhanVien,
      "#error_required_tenNhanVien",
      "Tên nhân viên"
    ) &
    kiemTra.kiemTraRong(
      nhanVienCapNhat.luongCoBan,
      "#error_required_luongCoBan",
      "Lương cơ bản"
    ) &
    kiemTra.kiemTraRong(
      nhanVienCapNhat.soGioLamTrongThang,
      "#error_required_soGioLamTrongThang",
      "Số giờ làm trong tháng"
    );

  // Kiểm tra loại dữ liệu
  valid &=
    kiemTra.kiemTraKiTu(
      nhanVienCapNhat.tenNhanVien,
      "#error_allLetters_tenNhanVien",
      "Tên nhân viên"
    ) &
    kiemTra.kiemTraKySo(
      nhanVienCapNhat.luongCoBan,
      "#error_allNumber_luongCoBan",
      "Lương cơ bản"
    ) &
    kiemTra.kiemTraKySo(
      nhanVienCapNhat.soGioLamTrongThang,
      "#error_allNumber_soGioLamTrongThang",
      "Số giờ làm trong tháng"
    );

  // Kiểm tra min max
  valid &=
    kiemTra.kiemTraMinMaxGiaTri(
      nhanVienCapNhat.luongCoBan,
      "#error_Min_Max_luongCoBan",
      20000000,
      1000000,
      "Lương cơ bản"
    ) &
    kiemTra.kiemTraMinMaxGiaTri(
      nhanVienCapNhat.soGioLamTrongThang,
      "#error_Min_Max_soGioLamTrongThang",
      150,
      50,
      "Số giờ làm trong tháng"
    );

  if (!valid) {
    return;
  }

  console.log("nhanVienCapNhat", nhanVienCapNhat);
  var promise = axios({
    url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/CapNhatThongTinNhanVien?maNhanVien=${nhanVienCapNhat.maNhanVien}`,
    method: "put",
    data: nhanVienCapNhat,
  });

  promise.then(function (result) {
    layDanhSachNhanVienAPI();
  });

  promise.catch(function (error) {
    console.log(error.response.data);
  });

  document.querySelector("#btnCapNhatNhanVien").style.display = "none";
  document.querySelector("#btnThemNhanVien").style.display = "inline-block";
  document.querySelector("#maNhanVien").disabled = false;
};
